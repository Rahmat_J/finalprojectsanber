import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Splash from './src/Modules';
import Login from './src/Modules/Authentication/Login';
import Dashboard from './src/Modules/Dashboard/Dashboard';
import Profile from './src/Modules/Profile/Profile';
import Menu from './src/Modules/Menu/Menu';
import DetailMenu from './src/Modules/Menu/DetailMenu';
import { Image } from 'react-native';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabScreen = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused
            ? 'home-sharp'
            : 'home-outline';
        } else if (route.name === 'Menu') {
          iconName = focused ? 'fast-food' : 'fast-food-outline';
        } else {
          iconName = focused ? 'person' : 'person-outline';
        }
        return <Ionicons name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#00FFF0',
      inactiveTintColor: '#FFFFFF',
      style: {
        backgroundColor: '#980000',
      }
    }}
  >
    <Tab.Screen
      name="Home"
      component={Dashboard}
    />
    <Tab.Screen name="Menu"
      component={Menu}
    />
    <Tab.Screen
      name="Profile"
      component={Profile}
    />
  </Tab.Navigator>
);


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="splash">
        <Stack.Screen
          name="splash"
          component={Splash}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="DetailMenu"
          component={DetailMenu}
        />
        <Stack.Screen
          name="Home"
          component={TabScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
