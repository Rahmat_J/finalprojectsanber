import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 60,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,
                        backgroundColor: '#148AA4',
                    }}>
                    <Text style={{ fontSize: 24, color: '#FFF' }}>Menu</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../../images/constructionxxxhdpi.png')} style={{ height: 300, width: 300 }} />
                    <Text style={{ fontSize: 18 }}>Coming Soon</Text>
                </View>
            </View>
        );
    }
}
