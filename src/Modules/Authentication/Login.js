import React, { Component } from 'react';
import { View, Text, Image, BackHandler, Alert } from 'react-native';
import { TextInput, ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isErrorUsername: false,
            isErrorPassword: false,
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp();
    };

    loginHandler() {
        const { route, navigation } = this.props;
        const { userName, password } = this.state;
        if (password === '123456' && userName !== '') {
            navigation.navigate('Home', { post: userName });
        } else if (userName === '') {
            this.setState({ isErrorUsername: true })
        } else {
            this.setState({ isErrorPassword: true })
            this.setState({ isErrorUsername: false })
        }
    }

    render() {
        const { userName, password, isErrorUsername, isErrorPassword } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <ScrollView>
                    <Image
                        source={require('../../images/hotfood.png')}
                        style={{ height: 187, width: 187, alignSelf: 'center', marginTop: 40 }}
                        resizeMode='stretch'
                    />
                    <View style={{ marginTop: 40, marginHorizontal: 30 }}>
                        <Text style={{ fontSize: 16 }}>Username</Text>
                        <TextInput
                            onChangeText={userName => this.setState({ userName })}
                            style={{
                                borderWidth: 1,
                                borderColor: 'grey',
                                borderRadius: 10,
                                padding: 10,
                                marginTop: 10,
                            }}
                        />
                        <Text style={{ color: isErrorUsername ? '#980000' : 'transparent', marginTop: 5 }}>Tidak Boleh Kosong!</Text>
                    </View>
                    <View style={{ marginTop: 20, marginHorizontal: 30 }}>
                        <Text style={{ fontSize: 16 }}>Password</Text>
                        <TextInput
                            onChangeText={password => this.setState({ password })}
                            style={{
                                borderWidth: 1,
                                borderColor: 'grey',
                                borderRadius: 10,
                                padding: 10,
                                marginTop: 10,
                            }}
                            secureTextEntry
                        />
                        <Text style={{ color: isErrorPassword ? '#980000' : 'transparent', marginTop: 5 }}>Password Salah</Text>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.6}
                        style={{ marginRight: 30, marginTop: 10 }}
                    >
                        <Text style={{ color: '#660000', textAlign: 'right' }}> Forgot Passowrd!</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={() => this.loginHandler()}
                        style={{
                            backgroundColor: '#660000',
                            borderRadius: 10, height: 45,
                            marginHorizontal: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginVertical: 30
                        }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 24 }}>Login</Text>
                    </TouchableOpacity>
                    <View style={{ marginBottom: 30, alignSelf: 'center', flexDirection: 'row' }}>
                        <Text>Already have an account?</Text>
                        <TouchableOpacity style={{ marginLeft: 5 }}>
                            <Text style={{ color: '#660000', fontSize: 14 }}>Register!</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
