import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator } from 'react-native';

export default class index extends Component {
    render() {
        // const {navigate} = this.props.navigation;
        setTimeout(() => {
            this.props.navigation.navigate('Login')
        }, 5000);

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#660000' }}>
                <Image style={{ alignSelf: 'center', justifyContent: 'center', height: 278, width: 278 }} source={require("../images/hotfood.png")} resizeMode='stretch' />
                <ActivityIndicator size={'large'} style={{ marginTop: 30 }} />
            </View>
        );
    }
}
