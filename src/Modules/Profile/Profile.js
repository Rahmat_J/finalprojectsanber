import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { navigation, route } = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 60,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,
                        backgroundColor: '#980000',
                    }}>
                    <Text style={{ fontSize: 24, color: '#FFF' }}>Profile</Text>
                </View>
                <View style={{ flexDirection: 'row', marginHorizontal: 30, marginVertical: 30 }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: '#C4C4C4' }}>
                        <Ionicons name="person" size={30} color="#000" />
                    </View>
                    <View style={{ marginLeft: 10, justifyContent: 'center' }}>
                        <Text style={{ fontSize: 18 }}>{`Hello, ${route.params?.post}`}</Text>
                        <Text>Guess</Text>
                    </View>
                </View>
                <View style={{ height: 3, borderWidht: 1, backgroundColor: '#636363', marginVertical: 30 }} />
                <View style={{ marginHorizontal: 30, marginBottom: 30 }}>
                    <Text style={{ fontSize: 18 }}>No. Hp</Text>
                    <View style={{ marginTop: 10, backgroundColor: '#BFBFBF', height: 45, borderRadius: 10, justifyContent: 'center', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 14 }}>082391xxxxxx</Text>
                    </View>
                </View>
                <View style={{ marginHorizontal: 30, marginBottom: 30 }}>
                    <Text style={{ fontSize: 18 }}>Jenis kelamin</Text>
                    <View style={{ marginTop: 10, backgroundColor: '#BFBFBF', height: 45, borderRadius: 10, justifyContent: 'center', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 14 }}>Laki-laki</Text>
                    </View>
                </View>
                <View style={{ marginHorizontal: 30, marginBottom: 30 }}>
                    <Text style={{ fontSize: 18 }}>Alamat</Text>
                    <View style={{ marginTop: 10, backgroundColor: '#BFBFBF', height: 45, borderRadius: 10, justifyContent: 'center', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 14 }}>Jl. Kalimantan No. 8</Text>
                    </View>
                </View>
                <View style={{ marginHorizontal: 30, marginBottom: 30 }}>
                    <Text style={{ fontSize: 18 }}>Sosial media</Text>
                    <View style={{ marginTop: 10, backgroundColor: '#BFBFBF', height: 45, borderRadius: 10, justifyContent: 'center', paddingHorizontal: 10 }}>
                        <Text style={{ fontSize: 14 }}>@rahmatjanuardi</Text>
                    </View>
                </View>
            </View>
        );
    }
}
