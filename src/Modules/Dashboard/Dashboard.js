import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { navigation, route } = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 60,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,
                        backgroundColor: '#FFFF',
                    }}>
                    <Image source={require('../../images/hotfood.png')} style={{ height: 50, width: 50 }} />
                </View>
                <View style={{ marginHorizontal: 30, marginVertical: 30 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: 60, height: 60, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: '#C4C4C4' }}>
                            <Ionicons name="person" size={30} color="#000" />
                        </View>
                        <View style={{ marginLeft: 10, justifyContent: 'center' }}>
                            <Text style={{ fontSize: 18 }}>{`Hello, ${route.params?.post}`}</Text>
                            <Text>Rp.0 ,-</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 65 }}>
                        <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                            <Text style={{ fontSize: 18 }}>Burger</Text>
                            <Text style={{ fontSize: 18, color: '#FFB800' }}>See All</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
